import { BitcoinProvider } from './../../providers/bitcoin/bitcoin';
import { GenericOption, Bitcoin } from './../../app/models/GenericOption';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs';


/**
 * Generated class for the BitcoinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bitcoin',
  templateUrl: 'bitcoin.html',
})
export class BitcoinPage {

   btcOptions : Array<GenericOption> = new Array() ;
   bitcoin$ : Observable<Bitcoin>;
   tipoMoneda: string = "GBP";

  constructor(private bitCoinProvider: BitcoinProvider) {

    this.btcOptions = bitCoinProvider.getTiposMoneda();


  }

  ionViewWillLoad() {
    this.getBitCoinPrice();
  }

  getBitCoinPrice(){
    this.bitcoin$ = this.bitCoinProvider.getBitcoinPrice(this.tipoMoneda);
  
  }

}

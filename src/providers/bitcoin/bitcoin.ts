import { Bitcoin } from './../../app/models/BitCoin';
import { GenericOption } from './../../app/models/GenericOption';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the BitcoinProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BitcoinProvider {

  private ROOT_URL= 'https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC';
  

  constructor(public http: HttpClient) {
  
  }

  getTiposMoneda() : Array<GenericOption>{
    return [{
        value: "GBP",
        text : "GBP"
      },
         {
        value: "USD",
        text : "USD"
      },
         {
        value: "AUD",
        text : "AUD"
      },
         {
        value: "EUR",
        text : "EUR"
      },
         {
        value: "INR",
        text : "INR"
         }];
  }

  getBitcoinPrice(tipoMoneda : string){

    return this.http.get<Bitcoin>(this.ROOT_URL+tipoMoneda);

  }

}
